/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;
import javax.swing.JOptionPane;

/**
 *
 * @author madar
 */
public class Prueba_punto_captura_Obligatorio {
    
    public static void main(String[] args) {
       
        
         String datos=JOptionPane.showInputDialog(null, "Digite PUNTOS SEPARADOS POR ,","Puntos", JOptionPane.QUESTION_MESSAGE);
         try{
            Punto p1=new Punto(datos);
            JOptionPane.showMessageDialog(null, "El punto fue:"+p1.toString());
         }catch(Exception e)
         {
             JOptionPane.showMessageDialog(null, e.getMessage());
         }
    }
}
